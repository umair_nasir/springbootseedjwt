package com.springbootseed.Persistence.Entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;
import org.joda.time.DateTime;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by umair.nasir on 3/23/17.
 */
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Session {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;

    Date sessionStart;
    Date sessionEnd;

    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    Login login;

    public void expireAfter(int secondsFromNow) {
        setSessionEnd(new DateTime().plusSeconds(secondsFromNow).toDate());
    }

}