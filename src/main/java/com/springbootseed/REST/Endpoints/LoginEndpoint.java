package com.springbootseed.REST.Endpoints;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * Created by umair.nasir on 3/24/17.
 */
@RestController
@RequestMapping("/login")
public class LoginEndpoint {

    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(method = RequestMethod.POST)
    public void login() {}

}
