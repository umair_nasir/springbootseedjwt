package com.springbootseed.REST.Mappers;

import com.springbootseed.Persistence.Entities.User;
import com.springbootseed.REST.DTOs.UserDto;
import org.springframework.stereotype.Service;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Created by umair.nasir on 3/23/17.
 */
@Service
public class UserMapper extends EntityMapper<User, UserDto> {

    @Override
    public UserDto toDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setUserName(user.getUserName());
        userDto.setPermissions(user.getPermissions());
        return userDto;
    }

    @Override
    public void updateFromDto(User user, UserDto userDto) {
        throw new NotImplementedException();
    }

    @Override
    public User createNewEntity() {
        return new User();
    }

}

