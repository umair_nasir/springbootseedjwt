package com.springbootseed.Services;


import com.springbootseed.Constants;
import com.springbootseed.Persistence.Entities.Login;
import com.springbootseed.Persistence.Entities.Session;
import com.springbootseed.Persistence.Entities.User;
import com.springbootseed.Persistence.Repositories.LoginRepo;
import com.springbootseed.Persistence.Repositories.SessionRepo;
import com.springbootseed.Services.Security.LoginContext;
import io.jsonwebtoken.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by umair.nasir on 3/22/17.
 */
@Service
public class AuthenticationService {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Value("${session.expiry_seconds}")
    private int SESSION_EXPIRY_SECONDS;
    @Value("${token.expiry_seconds}")
    private int EXPIRATION_TIME_IN_SECONDS;
    @Value("${token.secret_key}")
    private String SECRET;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private SessionRepo sessionRepo;

    @Autowired
    private LoginRepo loginRepo;


    public String authenticate(String userName, String password){
        Authentication authentication = new UsernamePasswordAuthenticationToken(userName, password);

        try {
            authentication = authenticationManager.authenticate(authentication);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            if (authentication.getPrincipal() != null) {
                LoginContext loginContext = (LoginContext) authentication.getPrincipal();
                Login login = loginContext.getLogin();

                String newSessionId = createNewSession(login);
                String newToken = createToken(login, newSessionId);
                return newToken;
            }
        } catch (AuthenticationException e){
            log.info("Authentication Failed");
            log.debug("Authentication Failed: "+e);
        }
        return null;
    }

    private String createToken(Login login, String sessionId) {
        User user = login.getUser();

        LoginClaim loginClaim = null;
        if (user != null) {
            loginClaim = new LoginClaim(login.getUser().getId(), login.getUser().getPermissions());
        }

        String JWT = Jwts.builder()
                .setSubject(login.getUserName())
                .setExpiration(DateTime.now().plusSeconds(EXPIRATION_TIME_IN_SECONDS).toDate())
                .claim(Constants.Session.SESSION_ID, sessionId)
                .claim(Constants.Session.LOGIN_CLAIM, loginClaim)
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();

        return JWT;
    }

    public String createNewSession(Login login) {
        Session session = new Session();
        session.setSessionStart(new Date());
        session.setSessionEnd(DateTime.now().plusSeconds(SESSION_EXPIRY_SECONDS).toDate());
        session.setLogin(login);

        return sessionRepo.saveAndFlush(session).getId();
    }

    public Login refreshSession(String sessionId) {
        Session session = sessionRepo.findOne(sessionId);

        if (session == null) {
            log.info("No session found with that token: Fail");
            return null;
        }
        Login login = session.getLogin();

        log.debug("Found login: " + login.getUserName());

        if (new Date().after(session.getSessionEnd())) {
            log.info("Session found, but expired.");
            return null;
        }

        session.expireAfter(SESSION_EXPIRY_SECONDS);
        sessionRepo.saveAndFlush(session);
        log.info("Successfully renewed session");
        return login;
    }

    public boolean endSession(String sessionId) {
        Session session = sessionRepo.findOne(sessionId);
        if (session == null) {
            log.warn("During logout, session ID not found: " + sessionId);
            return false;
        }
        Login login = session.getLogin();
        session.setSessionEnd(new Date());
        sessionRepo.saveAndFlush(session);

        log.info("Successfully logged out login: " + login.getUserName());
        return true;
    }

    private Object getCurrentPrincipal() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authentication == null? null : authentication.getPrincipal();
    }

    public void logout(String token) {
        Claims claims;
        try {
            claims = parse(token);
        } catch (ExpiredJwtException e) {
            claims = e.getClaims();
        } catch (SignatureException e) {
            // welp ...
            log.warn("Invalid token signature!");
            return;
        } catch (MalformedJwtException ex) {
            log.warn("Badly formed token!");
            return;
        }

        String sessionId = claims.get(Constants.Session.SESSION_ID, String.class);
        if(endSession(sessionId)){
            SecurityContextHolder.clearContext();
        }
    }

    private Claims parse(String token) {
        return Jwts.parser()
                .setSigningKey(SECRET)
                .parseClaimsJws(token)
                .getBody();
    }

    public String checkToken(String token) {
        log.info("Checking token: " + token);
        PreAuthenticatedAuthenticationToken securityToken;
        try{
            Claims body = parse(token);
            HashMap loginClaim = body.get(Constants.Session.LOGIN_CLAIM, HashMap.class);
            Collection<GrantedAuthority> authorities = new ArrayList<>();

            if(loginClaim != null) {
                int permissions = (int) loginClaim.get(Constants.Session.PERMISSIONS);
                if ((permissions & Constants.Permissions.ADMIN) > 0)
                    authorities.addAll(LoginContext.getAdminAuthorities());

                if ((permissions & Constants.Permissions.USER) > 0) {
                    authorities.addAll(LoginContext.getUserAuthorities());
                }
            }
            log.info("Token accepted");
            log.debug("Granting authorities: " + authorities);
            securityToken = new PreAuthenticatedAuthenticationToken(token, null, authorities);
        }catch (ExpiredJwtException ex){
            log.info("Token expired");
            Claims claims = ex.getClaims();

            String sessionId = (String)claims.get(Constants.Session.SESSION_ID);
            Login login = refreshSession(sessionId);
            if (login == null) return null;

            UserDetails userDetails = new LoginContext(login);
            log.debug("Granting authorities: " + userDetails.getAuthorities());
            token = createToken(login, sessionId);
            log.info("Issuing new token: " + token);

            securityToken = new PreAuthenticatedAuthenticationToken(
                    userDetails, null, userDetails.getAuthorities());
        }catch (SignatureException e) {
            log.warn("Invalid token signature!");
            return null;
        } catch (MalformedJwtException ex) {
            log.warn("Badly formed token!");
            return null;
        }

        SecurityContextHolder.getContext().setAuthentication(securityToken);
        return token;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    private class LoginClaim {
        String id;
        int permissions;
    }
}
