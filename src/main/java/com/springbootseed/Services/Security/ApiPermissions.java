package com.springbootseed.Services.Security;


public class ApiPermissions {
    public static final String ADMINISTER_ORGANIZATION_ANNOTATION = "hasRole('" + Permissions.ADMINISTER_ORGANIZATION + "')";
    public static final String USER_ORGANIZATION_ANNOTATION = "hasRole('" + Permissions.USER_ORGANIZATION + "')";
}
