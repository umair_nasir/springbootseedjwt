package com.springbootseed.Services.Security;

import com.springbootseed.Services.AuthenticationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Service
public class JWTAuthenticationFilter extends GenericFilterBean {

    private static final String LOGOUT = "/logout";
    private static final String LOGIN = "/login";
    private Logger log = LoggerFactory.getLogger(this.getClass());

    @Value("${token.header}")
    private String HEADER_STRING;

    @Autowired
    AuthenticationService authenticationService;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain)
            throws IOException, ServletException {

        HttpServletRequest httpRequest = (HttpServletRequest) request;
        HttpServletResponse httpResponse = (HttpServletResponse) response;

        String newToken = checkToken(httpRequest, httpResponse);
        boolean authenticated = newToken != null;

        if (currentLink(httpRequest).equals(LOGIN) && !authenticated) {
            doLogin(httpRequest, httpResponse);
        }
        if (currentLink(httpRequest).equals(LOGOUT) && authenticated) {
            doLogout(httpRequest, httpResponse);
        }

        filterChain.doFilter(request, response);
    }

    private void doLogin(HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
        String userName = httpRequest.getParameter("username");
        String password = httpRequest.getParameter("password");
        String JWT = authenticationService.authenticate(userName, password);
        if (JWT != null && httpResponse.getHeader(HEADER_STRING) == null) {
            httpResponse.addHeader(HEADER_STRING, JWT);
        }
    }

    private void doLogout(HttpServletRequest httpRequest, HttpServletResponse httpResponse) {
        String token = httpRequest.getHeader(HEADER_STRING);
        if (token == null)
            return;
        authenticationService.logout(token);
    }

    private String currentLink(HttpServletRequest httpRequest) {
        if (httpRequest.getPathInfo() == null) {
            return httpRequest.getServletPath();
        }
        return httpRequest.getServletPath() + httpRequest.getPathInfo();
    }

    private String checkToken(HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws IOException {
        String token = httpRequest.getHeader(HEADER_STRING);

        if (token == null)
            return null;

        String newToken = authenticationService.checkToken(token);

        if (newToken == null) {
            log.debug("Setting response code: unauthorized");
            httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED);;
        }

        httpResponse.setHeader(HEADER_STRING, newToken);
        return newToken;
    }
}