package com.springbootseed;

import com.springbootseed.Services.AuthenticationService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.TestPropertySource;

import static org.assertj.core.api.Java6Assertions.assertThat;

/**
 * Created by umair.nasir on 3/24/17.
 */

@TestPropertySource(properties = {"session.expiry_seconds = 2", "token.expiry_seconds = 1" })
public class AuthenticationTests extends TestBase {

    @Autowired
    private AuthenticationService authenticationService;

    private static final String CORRECT_EMAIL = "defaultUser";
    private static final String CORRECT_PASSWORD = "123321";

    @Test
    public void testGoodCredentials(){
        String loginResult = authenticationService.authenticate(CORRECT_EMAIL, CORRECT_PASSWORD);
        assertThat(loginResult).isNotNull();
        assertThat(authenticationService.checkToken(loginResult)).isNotNull();
    }

    @Test
    public void testIncorrectUserName() {
        String loginResult = authenticationService.authenticate("wrongUser", CORRECT_PASSWORD);
        assertThat(loginResult).isNull();
    }

    @Test
    public void testIncorrectPassword() {
        String loginResult = authenticationService.authenticate(CORRECT_EMAIL, "wrongPassword");
        assertThat(loginResult).isNull();
    }

    @Test
    public void testFakeToken() {
        assertThat(authenticationService.checkToken("fakeToken")).isNull();
    }

    @Test
    public void testThatSessionsExpire() {
        String loginResult = authenticationService.authenticate(CORRECT_EMAIL, CORRECT_PASSWORD);
        assertThat(loginResult).isNotNull();
        assertThat(authenticationService.checkToken(loginResult)).isNotNull();
        try {
            Thread.sleep(3000);
            assertThat(authenticationService.checkToken(loginResult)).isNull();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
